package com.spigot.explosivearrow;


import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	
	
	
	private boolean toggle = false;
	private float power;
	
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }
    
    @Override
    public boolean onCommand(CommandSender sender,
            Command command,
            String label,
            String[] args) {
        if (command.getName().equalsIgnoreCase("tntsnow")) {
        	if (args.length == 0) {
        		if (power == 0 || toggle == false)
        		sender.sendMessage("You have normal snowballs");
        		else 
        			sender.sendMessage("You have explosive snowballs with power " + power);
        		
        	} else if (args.length > 1) {
        		sender.sendMessage("/tntsnow + <power|0-10>");
        	} 
        	else {
        		power = Float.parseFloat(args[0]);
        		if (power < 0 && power > 10) {
        			sender.sendMessage("Power can only be between 0 and 10");
        		} else if (power == 0) {
        			toggle = false;
        			sender.sendMessage("You have normal snowballs");
        		} else {
        			toggle = true;
        			sender.sendMessage("You have explosive snowballs with power " + power);
        		}
        	}
        return true;
       } 
        return false;
    }

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent event) {
        Projectile projectile = event.getEntity();
        if (projectile instanceof Snowball && toggle) {
            Snowball snowball = (Snowball) projectile;
            snowball.getWorld().createExplosion(snowball.getLocation(), power);
        }
    }

}