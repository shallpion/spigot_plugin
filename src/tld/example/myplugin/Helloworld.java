package tld.example.myplugin;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Helloworld implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender,
            Command command,
            String label,
            String[] args) {
        if (command.getName().equalsIgnoreCase("helloworld")) {
            sender.sendMessage("Hello World!");
            return true;
        }
        return false;
    }
}
