package tld.example.myplugin;


import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import tld.example.myplugin.Helloworld;
import tld.example.myplugin.Lol;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
    @Override
    public void onEnable() {
    	this.getCommand("helloworld").setExecutor(new Helloworld());
    	this.getCommand("lol").setExecutor(new Lol());
       
    }
   
    @Override
    public void onDisable() {
       
    }

}
